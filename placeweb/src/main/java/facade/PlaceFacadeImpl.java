package facade;

import entity.Concert;
import entity.Restaurant;
import service.PlaceService;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
@Default
public class PlaceFacadeImpl implements PlaceFacade {

    @Inject
    private PlaceService placeService;


    @Override
    public List<Concert> getConcerts() {
        return placeService.getConcerts();
    }

    @Override
    public List<Restaurant> getRestaurants(long id) {
        return placeService.getRestaurants(id);
    }
}
