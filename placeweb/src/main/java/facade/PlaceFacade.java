package facade;

import entity.Concert;
import entity.Restaurant;

import java.util.List;

public interface PlaceFacade {

    List<Concert> getConcerts();
    List<Restaurant>getRestaurants(long id);
}
