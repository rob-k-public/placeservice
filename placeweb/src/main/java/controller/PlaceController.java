package controller;

import entity.Concert;
import entity.Restaurant;
import facade.PlaceFacade;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.List;

@Path("/place")
public class PlaceController {

    @Inject
    private PlaceFacade facade;

    @GET
    @Path("/health")
    public String getHealth(){
        return "Service is running (faster than you).";
    }

    @GET
    @Path("/concerts")
    public List<Concert> getAllConcerts(){
        return facade.getConcerts();
    }

    @GET
    @Path("/restaurants/{concertId}")
    public List<Restaurant>getRestaurants(@QueryParam("concertId")long id){
        return facade.getRestaurants(id);
    }
}
