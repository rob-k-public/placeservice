package entity;

import java.util.Collections;
import java.util.List;

public class Restaurants {

    private List<Restaurant> results = Collections.emptyList();

    public Restaurants() {
    }

    public List<Restaurant> getResults() {
        return results;
    }

    public void setResults(List<Restaurant> results) {
        this.results = results;
    }
}
