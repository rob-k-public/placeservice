package entity;

import java.util.Collections;
import java.util.List;

public class Concerts {

    private List<Concert> items = Collections.emptyList();

    public Concerts() {
    }

    public List<Concert> getItems() {
        return items;
    }

    public void setItems(List<Concert> items) {
        this.items = items;
    }
}
