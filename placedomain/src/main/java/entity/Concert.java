package entity;

import java.util.Date;

public class Concert {

    private String artist;
    private Date date;
    private Place place;

    public Concert(String artist, Date date, Place place) {
        this.artist = artist;
        this.date = date;
        this.place = place;
    }

    public Concert() {
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
