package gateway;

import entity.Concert;
import entity.Concerts;

import java.io.IOException;

public interface ConcertGateway {

    Concerts getConcerts() throws IOException;
    Concert getConcert(long id) throws IOException;
}
