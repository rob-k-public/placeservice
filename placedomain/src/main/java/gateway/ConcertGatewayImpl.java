package gateway;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Concert;
import entity.Concerts;
import entity.Place;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Model;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@Named
@RequestScoped
@Default
@Model
public class ConcertGatewayImpl implements ConcertGateway {

    private Client client;
    private String path = "http://193.191.187.14:11008/concertweb/rest/concert";


    @Override
    public Concerts getConcerts() throws IOException {
        client = ClientBuilder.newClient();

        WebTarget target = client.target(path + "/all");
        Response response = target.request("application/json").get();
        String json = response.readEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(json, Concerts.class);
    }

    @Override
    public Concert getConcert(long id) throws IOException {
        client = ClientBuilder.newClient();

        WebTarget target = client.target(path + "/find/" + id);
        Response response = target.request("application/json").get();
        String json = response.readEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Optional<Concert>concertOptional = Optional.ofNullable(mapper.readValue(json, Concert.class));
        return concertOptional.orElseGet(() -> new Concert("Drake", new Date(), new Place("Kerk", "Oevel", "BE")));
    }
}
