package gateway;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Restaurants;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Model;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Named
@RequestScoped
@Default
@Model
public class RestaurantGatewayImpl implements RestaurantGateway {

    private Client client;
    private String path = "https://maps.googleapis.com/maps/api/place/textsearch";
    private String query = "restaurants%20in%20";
    private String key = "AIzaSyAMf_-oytQIH44N2-9TpIJQSZCl34_XhPI";

    @Override
    public Restaurants getRestaurants(String city) throws IOException {
        client = ClientBuilder.newClient();

        WebTarget target = client.target(path + "/json")
                .queryParam("key", key)
                .queryParam("query", query + city);
        Response response = target.request("application/json").get();
        String json = response.readEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(json, Restaurants.class);
    }
}
