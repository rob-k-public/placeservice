package gateway;

import entity.Restaurants;

import java.io.IOException;

public interface RestaurantGateway {

    Restaurants getRestaurants(String city) throws IOException;
}
