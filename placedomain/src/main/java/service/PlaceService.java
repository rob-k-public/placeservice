package service;

import entity.Concert;
import entity.Restaurant;

import java.util.List;

public interface PlaceService {

    List<Concert>getConcerts();
    List<Restaurant> getRestaurants(long id);
    Concert getById(long id);
}
