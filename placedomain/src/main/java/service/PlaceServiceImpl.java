package service;

import entity.Concert;
import entity.Restaurant;
import gateway.ConcertGateway;
import gateway.RestaurantGateway;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Named
@RequestScoped
@Default
public class PlaceServiceImpl implements PlaceService {

    @Inject
    private ConcertGateway concertGateway;
    @Inject
    private RestaurantGateway restaurantGateway;

    @Override
    public List<Concert> getConcerts() {
        try {
            return concertGateway.getConcerts().getItems();
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Restaurant> getRestaurants(long id) {
        try {
            Concert concert = concertGateway.getConcert(id);
            return restaurantGateway.getRestaurants(concert.getPlace().getCity()).getResults();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @Override
    public Concert getById(long id) {
        try {
            return concertGateway.getConcert(id);
        } catch (IOException e) {
            return null;
        }
    }


}
